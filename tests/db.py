import os
import socket
import datetime
import psycopg2
import logging
from psycopg2.extras import RealDictCursor

logging.basicConfig(level=logging.DEBUG,
                    format='[%(asctime)s] {}/%(levelname)s/%(filename)s: %(message)s'.format(socket.gethostname()))

try:
    connection = psycopg2.connect(
        host=       os.environ['PGHOST'],
        dbname=     os.environ['PGDATABASE'],
        user=       os.environ['PGUSER'],
        password=   os.environ['PGPASSWORD'],
        port=       os.environ['PGPORT'],
    )
    cursor = connection.cursor(cursor_factory=RealDictCursor)
    logging.info(f'Connected to database: {connection.dsn}')

except psycopg2.Error as db_err:
    logging.info('Database connection failed')

def get_scenarios():
    logging.info('db.get_scenarios() called')
    sql =   '''
                SELECT 
                    "LastName",
                    "FirstName",
                    "Gender",
                    "BirthDate",
                    "PostalCode",
                    "HealthInsuranceNumber",
                    "FirstDoseDate" AS "FirstVaccinationDate",
                    "TradeName" AS "VaccineManufacturer"
                FROM public."TestScenarios" ts
                WHERE ts."FirstDoseDate" NOTNULL
            '''
    
    cursor.execute(sql)
    rows = cursor.fetchall()

    # format any date to yyyy-mm-dd
    for row in rows:
        for key in row:
            if isinstance(row[key], datetime.datetime):
                row[key] = row[key].strftime('%Y-%m-%d')

    return rows
