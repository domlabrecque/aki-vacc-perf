import time
import db
import logging
import random
from locust import HttpUser, task, between

data = db.get_scenarios()

class vaxUser(HttpUser):
    wait_time = between(1,5)
    host = 'https://aki-vacc-ca-stg.akinox.dev'
    endpoint_request = '/api/v1.3/candidates/request'
    endpoint_get = '/api/v1.3/candidates/get'

    @task
    def request(self):
        candidate = random.choice(data)
        hin = candidate['HealthInsuranceNumber']

        # initial request
        r = self.client.post(self.endpoint_request, json=candidate, name=self.endpoint_request)
        if r.status_code != 200:
            logging.info(f'request error - status code: {r.status_code}')
            return
        requestID = r.json()['candidateRequestId']
        json_get = { "candidateRequestId": requestID }
        logging.info(f'request - HIN: {hin} requestID: {requestID}')

        # polling
        for attemptNo in range(9):
            r = self.client.post(self.endpoint_get, json=json_get, name=self.endpoint_get)
            logging.info(f'get - {requestID} - attempt: {attemptNo} - status: {r.status_code}')
            if r.status_code == 200:
                break
            time.sleep(2)